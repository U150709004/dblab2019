SELECT 
    title
FROM
    movies
WHERE
    budget > 10000000 AND ranking < 6;

SELECT 
    movies.movie_id, title
FROM
    movies
        JOIN
    genres ON movies.movie_id = genres.movie_id
WHERE
    rating > 8 AND year > 1990
        AND genre_name = 'Action';


SELECT 
    title
FROM
    movies
WHERE
    duration > 150 AND oscars > 2
        AND movie_id IN (SELECT 
            movie_id
        FROM
            genres
        WHERE
            genre_name = 'Drama');


SELECT 
    movie_id, title
FROM
    movies
WHERE
    oscars > 2
        AND movie_id IN (SELECT 
            movie_id
        FROM
            movie_stars
                JOIN
            stars ON movie_stars.star_id = stars.star_id
        WHERE
            star_name = 'Orlando Bloom'
                AND movie_id IN (SELECT 
                    movie_id
                FROM
                    movie_stars
                        JOIN
                    stars ON movie_stars.star_id = stars.star_id
                WHERE
                    star_name = 'Ian McKellen'));
                    
#Q5
select title
from movies
where votes > 500000 and year < 2000 and movie_id In
(select movie_id 
from movie_directors join directors on movie_directors.director_id = directors.director_id
where director_name = "Quentin Tarantino");

#Q6
select title
from movies natural join genres 
where budget > 25000000 and genre_name = "thriller";

#Q7
select title
from movies
where year between 1990 and 2000 and movie_id in (select languages.movie_id 
from languages join genres on languages.movie_id=genres.movie_id
where language_name = "Italian" and genre_name="Drama");

#Q8
select title
from movies
where oscars > 3 and movie_id in (
 select movie_id
 from movie_stars join stars on movie_stars.star_id = stars.star_id
 where star_name like "%Hanks");
 
#Q9
select title
from movies
where duration between 100 and 200 and movie_id in
(select genres.movie_id
from (genres join producer_countries on genres.movie_id = producer_countries.movie_id) join countries on producer_countries.country_id=countries.country_id
where genre_name = "History" and country_name = "USA");

#Q10
